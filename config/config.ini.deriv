# Planet configuration file

# The following rules apply for a new language planet:
#
# * The feeds contain the same content as one would put on planet.debian.org,
#   ie. mostly Debian related / from people involved in Debian
#
# * The feeds provide an own category with only the language for this planet.
#
# * At least 10 feeds have to be there before a new language gets added.
#
# * Language planets will appear as planet.debian.org/$LANGUAGE,
#   where $LANGUAGE will be the two-letter code for it.


# Little documentation for multi-language planet:

# In the following, above "Do not change", replace $LANGUAGE with the
# name of your language, $2LETTERCODE with the 2 letter iso code.
# For example, a german planet would use Deutsch and DE.
# Feel free to edit the other values as shown.
# Please do not touch the config values below "Do not change", just
# skip to the feed list.

# When you are done, send this file to planet@debian.org and ask for the
# addition of the new language planet. Do NOT just commit it, it won't get
# picked up by the scripts.

# After the new language is activated, feel free to edit this file following
# the normal rules for all planet config.ini files.

# Hint: You can use hackergotchis the same way as on main planet. But it is
# *one* central storage for hackergotchis, not one per different language.


# Every planet needs a [Planet] section
[Planet]
# name: Your planet's name.
name = Planet Debian Derivatives
# link: Link to the main planet page
link = https://planet.debian.org/deriv/
# language: short language code for the <html ... lang=""> tag
language = en

# output_dir: Directory to place output files. Add your 2letter Language code
output_dir = www/deriv
# date_format: strftime format for the default 'date' template variable
date_format = %d %B, %Y %I:%M%p
# new_date_format: strftime format for the 'new_date' template variable
new_date_format = %B %d, %Y
# cache_directory: Where cached feeds are stored. Add your 2letter Language code
cache_directory = cache/deriv

# Translate the following to your language. Do not change the names, just the
# text after the =
syndicationtext = A complete feed is available in any of your favourite syndication formats linked by the buttons below.
searchtext = Search
lastupdatetext = Last updated:
utctext = All times are UTC.
contacttext = Contact:
hiddenfeedstext = Hidden Feeds
hiddenfeedstext2 = You currently have hidden entries.
showalltext = Show all
subscriptionstext = Subscriptions
feedtext = feed
otherplanettext = Planetarium


# Do not change config values below here, just skip to the feeds
# Do not change config values below here, just skip to the feeds
owner_name = Debian Planet Maintainers
owner_email = planet@debian.org

# Currently no search for Language planets
search = false
# new_feed_items: Number of items to take from new feeds
# log_level: One of DEBUG, INFO, WARNING, ERROR or CRITICAL
new_feed_items = 50
log_level = DEBUG
spider_threads = 30

# template_files: Space-separated list of output template files
template_files = git/debian/templates/index.html.dj git/debian/templates/atom.xml.dj git/debian/templates/rss20.xml.dj git/debian/templates/rss10.xml.dj git/debian/templates/opml.xml.dj git/debian/templates/foafroll.xml.dj

# The following provide defaults for each template:
# items_per_page: How many items to put on each page
# days_per_page: How many complete days of posts to put on each page
#                This is the absolute, hard limit (over the item limit)
# encoding: output encoding for the file, Python 2.3+ users can use the
#           special "xml" value to output ASCII with XML character references
# locale: locale to use for (e.g.) strings in dates, default is taken from your
#         system
items_per_page = 60
days_per_page = 0
encoding = utf-8
# locale = C

filters = remove-trackers-and-ads.plugin
filter_dir = code/filters

[git/debian/templates/index.html.dj]
date_format = %I:%M%P


# Options placed in the [DEFAULT] section provide defaults for the feed
# sections.  Placing a default here means you only need to override the
# special cases later.
[DEFAULT]
# Hackergotchi default size.
# If we want to put a face alongside a feed, and it's this size, we
# can omit these variables.
facewidth = 65
faceheight = 85
future_dates = ignore_date


############################## FEEDS ##############################
#
#                      DO    NOT    EDIT
#
# This section is generated from the Debian derivatives census:
# https://wiki.debian.org/Derivatives/Census
# https://wiki.debian.org/Derivatives/Integration
#
###################################################################

[https://jonathancarter.org/feed/]
name = AIMS Desktop developers
face = deriv/AIMS_Desktop
facewidth = 100
faceheight = 21

[http://feeds.feedblitz.com/alienvaultotx&x=1]
name = AlienVault OSSIM
face = deriv/AlienVault-OSSIM
facewidth = 100
faceheight = 62

[http://aptosid.com/index.php?module=News&func=view&theme=rss]
name = aptosid
face = deriv/Aptosid
facewidth = 100
faceheight = 30

[https://arc-team-open-research.blogspot.com/feeds/posts/default]
name = ArcheOS
face = deriv/ArcheOS
facewidth = 80
faceheight = 80

[https://www.armbian.com/feed/]
name = ARMBIAN
face = deriv/Armbian
facewidth = 100
faceheight = 25

[http://www.bccd.net/blog/feed]
name = BCCD
face = deriv/BCCD
facewidth = 100
faceheight = 49

[http://serambi.blankonlinux.or.id/atom.xml]
name = Blankon developers
face = deriv/BlankOn
facewidth = 100
faceheight = 23

[https://www.bunsenlabs.org/feed/news/atom]
name = BunsenLabs Linux

[https://sourceforge.net/p/clonezilla/news/feed.rss]
name = Clonezilla live
face = deriv/Clonezilla
facewidth = 50
faceheight = 71

[http://www.lis-blog.com/feed/]
name = CoreBiz
face = deriv/CoreBiz
facewidth = 100
faceheight = 37

[https://cumulusnetworks.com/blog/feed/]
name = Cumulus Linux

[http://cyborg.ztrela.com/feed/]
name = Cyborg Linux
face = deriv/CyborgLinux
facewidth = 100
faceheight = 33

[https://www.deepin.org/en/feed/]
name = Deepin
face = deriv/Deepin
facewidth = 32
faceheight = 32

[http://blog.doudoulinux.org/feed/atom]
name = DoudouLinux
face = deriv/DoudouLinux
facewidth = 100
faceheight = 48

[http://emmabuntus.sourceforge.net/blog/feed/]
name = Emmabuntüs Debian Edition
face = deriv/EmmabuntusDE
facewidth = 100
faceheight = 100

[https://blog.finnix.org/feed.xml]
name = Finnix
face = deriv/Finnix
facewidth = 100
faceheight = 29

[https://www.greenbone.net/en/feed/]
name = GreenboneOS
face = deriv/GreenboneOS
facewidth = 71
faceheight = 100

[https://blog.grml.org/feeds/index.rss2]
name = Grml
face = deriv/Grml
facewidth = 100
faceheight = 47

[http://planet.grml.org/rss20.xml]
name = Grml developers
face = deriv/Grml
facewidth = 100
faceheight = 47

[https://bugs-huayra.conectarigualdad.gob.ar/projects/huayra-gnu-linux/news.atom]
name = Huayra GNU/Linux developers
face = deriv/Huayra
facewidth = 100
faceheight = 54

[https://www.kali.org/feed/]
name = Kali Linux
face = deriv/Kali
facewidth = 100
faceheight = 19

[http://knopper.net/news.rss]
name = KNOPPIX
face = deriv/Knoppix
facewidth = 95
faceheight = 100

[http://blog.linuxmint.com/?feed=rss2]
name = LMDE

[https://www.it-muenchen-blog.de/index.php/feed/]
name = LiMux

[http://lihuen.linti.unlp.edu.ar/index.php?title=Noticias&action=feed&feed=rss]
name = Lihuen
face = deriv/Lihuen
facewidth = 100
faceheight = 100

[http://maemo.org/news/planet-maemo/rss.xml]
name = Maemo developers
face = deriv/Maemo
facewidth = 100
faceheight = 23

[https://www.netrunner.com/feed/]
name = Netrunner
face = deriv/Netrunner
facewidth = 100
faceheight = 21

[https://osmc.tv/rss/]
name = OSMC
face = deriv/OSMC
facewidth = 100
faceheight = 100

[http://www.pardus.org.tr/feed/]
name = Pardus
face = deriv/Pardus
facewidth = 100
faceheight = 40

[https://blog.parrotsec.org/feed/]
name = Parrot Security
face = deriv/ParrotSecurity
facewidth = 90
faceheight = 79

[http://forum.proxmox.com/external.php?type=RSS2&forumids=7]
name = Proxmox VE
face = deriv/Proxmox
facewidth = 100
faceheight = 15

[https://puri.sm/feed/]
name = Purism PureOS
face = deriv/Purism
facewidth = 100
faceheight = 21

[https://qlustar.com/rss.xml]
name = Qlustar
face = deriv/Qlustar
facewidth = 100
faceheight = 38

[https://www.qubes-os.org/feed.xml]
name = Qubes
face = deriv/Qubes
facewidth = 100
faceheight = 100

[https://www.supergrubdisk.org/feed/]
name = rescatux
face = deriv/Rescatux
facewidth = 100
faceheight = 81

[https://www.stamus-networks.com/feed/]
face = deriv/SELKS
facewidth = 100
faceheight = 35

[http://blog.salentos.it/feed/]
name = SalentOS
face = deriv/SalentOS
facewidth = 100
faceheight = 100

[http://www.debian-srbija.iz.rs/feeds/posts/default/-/serbian]
name = Serbian GNU/Linux
face = deriv/SerbianLinux
facewidth = 100
faceheight = 23

[https://solydxk.com/feed/]
name = SolydXK
face = deriv/SolydXK
facewidth = 100
faceheight = 54

[https://sparkylinux.org/feed/]
name = SparkyLinux
face = deriv/SparkyLinux
facewidth = 100
faceheight = 100

[https://tails.boum.org/news/index.en.rss]
name = Tails
face = deriv/Tails
facewidth = 100
faceheight = 100

[http://planet.tanglu.org/atom.xml]
name = Tanglu developers
face = deriv/Tanglu
facewidth = 100
faceheight = 33

[http://torios.top/feed/]
face = deriv/ToriOS
facewidth = 100
faceheight = 100

[https://www.turnkeylinux.org/blog/feed]
name = TurnKey Linux
face = deriv/TurnKeyLinux
facewidth = 90
faceheight = 92

[https://fridge.ubuntu.com/feed/]
name = Ubuntu
face = deriv/Ubuntu
facewidth = 100
faceheight = 23

[http://planet.ubuntu.com/rss20.xml]
name = Ubuntu developers
face = deriv/Ubuntu
facewidth = 100
faceheight = 23

[https://www.univention.com/news/blog-en/feed/]
name = Univention Corporate Server
face = deriv/UniventionCorporateServer
facewidth = 100
faceheight = 24

[http://www.punknix.com/node/feed]
name = Voyage Linux developers
face = deriv/VoyageLinux
facewidth = 100
faceheight = 31

[http://blog.vyos.net/posts.atom]
name = VyOS
face = deriv/VyOS
facewidth = 100
faceheight = 47

[http://blog.wazo.community/feeds/all.atom.xml]
name = Wazo
face = deriv/Wazo
facewidth = 95
faceheight = 25

[http://webconverger.org/blog/index.rss]
name = Webconverger
face = deriv/Webconverger
facewidth = 100
faceheight = 11

[https://www.whonix.org/blog/feed]
name = Whonix
face = deriv/Whonix
facewidth = 100
faceheight = 97

[https://anonymousoperatingsystem.wordpress.com/feed/]
name = Whonix developers
face = deriv/Whonix
facewidth = 100
faceheight = 97

[http://www.zevenos.com/feed]
name = Neptune
face = deriv/ZevenOS-Neptune
facewidth = 100
faceheight = 12

[https://www.zevenet.com/feed]
name = ZEVENET
face = deriv/Zevenet
facewidth = 100
faceheight = 19

[http://www.ev3dev.org/news/atom.xml]
name = ev3dev
face = deriv/ev3dev
facewidth = 100
faceheight = 100

[https://thegnewsenseblog.wordpress.com/feed/]
name = gNewSense
face = deriv/gNewSense
facewidth = 100
faceheight = 30

[http://semplice-linux.org/blog/rss]
name = Semplice
face = deriv/semplice
facewidth = 100
faceheight = 22

[https://news.siduction.org/feed/]
name = siduction
face = deriv/siduction
facewidth = 100
faceheight = 27

[https://xanadulinux.wordpress.com/feed/]
name = Xanadu
face = deriv/xanadu
facewidth = 100
faceheight = 100

[https://sinfallas.wordpress.com/feed/]
name = Xanadu developers
face = deriv/xanadu
facewidth = 100
faceheight = 100
